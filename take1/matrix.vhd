--Standard includes
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


-- Za ucf 	A0-A7 row pins
--				A8-A15 col pins

--Entity definiton
entity First is
    Port ( A : out  STD_LOGIC_VECTOR (15 downto 0); -- maps to pins 0 to 16 of port A
           clk, clk2,switch : in STD_LOGIC); --clock signal
end First;
 
 
--architecture definition
architecture Behavioral of First is
 --Interial signals
 signal row : STD_LOGIC_VECTOR(7 downto 0) := "10000000";               --signal for LED rows
 signal col : STD_LOGIC_VECTOR(7 downto 0) := "00000000";   
 --signal for LED columns
begin

--s1:entity work.freqDivGen port map(clk,clk2);

	--Proces za titranje, takt ledicama, izmjena redaka
    --Process Definition
	--prikaz stanja araraya se radi ovdje  
   process(clk)
   begin
        -- triggers action on rising edge of clock signal
     if rising_edge(clk) then
		case row is
			when "10000000" => row <= "01000000";
			when "01000000" => row <= "00100000";	
			when "00100000" => row <= "00010000";
			when "00010000" => row <= "00001000";
			when "00001000" => row <= "00000100";
			when "00000100" => row <= "00000010";	
			when "00000010" => row <= "00000001";
			when "00000001" => row <= "10000000";
		when others => NULL;
		end case;

		--increment counter
     end if;
   end process;
   

	--Proces za pisanje slika na zaslon
    --Process Definition
	--prikaz stanja araraya se radi ovdje  
   process(clk)
   begin
        -- triggers action on rising edge of clock signal
     if rising_edge(clk) then
		A(7 downto 0) <= row;
		A(15 downto 8 ) <=NOT col;
		
		--increment counter
     end if;
   end process;
   
   
   
	--Proces za izmjenu signala prema zaslonu
	--popunjavanje arraya se radi ovdje
   process(clk2)
   begin
        -- triggers action on rising edge of clock signal
     if rising_edge(clk2) then
		if(switch='0') then
		  col<= ((col+col)+1);
        end if;
		end if;
	end process;
 
 
 
end Behavioral;

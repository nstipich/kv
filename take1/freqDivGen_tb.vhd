--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:05:24 01/15/2019
-- Design Name:   
-- Module Name:   F:/Users/Nikola/take1/freqDivGen_tb.vhd
-- Project Name:  take1
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: freqDivGen
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY freqDivGen_tb IS
END freqDivGen_tb;
 
ARCHITECTURE behavior OF freqDivGen_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT freqDivGen
    PORT(
         clk : IN  std_logic;
         reset : IN  std_logic;
         clock_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';

 	--Outputs
   signal clock_out : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
   constant clock_out_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: freqDivGen PORT MAP (
          clk => clk,
          reset => reset,
          clock_out => clock_out
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   clock_out_process :process
   begin
		clock_out <= '0';
		wait for clock_out_period/2;
		clock_out <= '1';
		wait for clock_out_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;

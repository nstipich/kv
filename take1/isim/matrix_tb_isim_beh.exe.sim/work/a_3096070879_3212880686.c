/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "F:/Users/Nikola/take1/matrix.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
char *ieee_p_2592010699_sub_1837678034_503743352(char *, char *, char *, char *);
char *ieee_p_3620187407_sub_436279890_3965413181(char *, char *, char *, char *, int );
char *ieee_p_3620187407_sub_767668596_3965413181(char *, char *, char *, char *, char *, char *);


static void work_a_3096070879_3212880686_p_0(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    char *t5;
    int t6;
    char *t7;
    char *t8;
    int t9;
    char *t10;
    int t12;
    char *t13;
    int t15;
    char *t16;
    int t18;
    char *t19;
    int t21;
    char *t22;
    int t24;
    char *t25;
    int t27;
    char *t28;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;

LAB0:    xsi_set_current_line(29, ng0);
    t1 = (t0 + 1152U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3648);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(30, ng0);
    t3 = (t0 + 1512U);
    t4 = *((char **)t3);
    t3 = (t0 + 5670);
    t6 = xsi_mem_cmp(t3, t4, 8U);
    if (t6 == 1)
        goto LAB6;

LAB15:    t7 = (t0 + 5678);
    t9 = xsi_mem_cmp(t7, t4, 8U);
    if (t9 == 1)
        goto LAB7;

LAB16:    t10 = (t0 + 5686);
    t12 = xsi_mem_cmp(t10, t4, 8U);
    if (t12 == 1)
        goto LAB8;

LAB17:    t13 = (t0 + 5694);
    t15 = xsi_mem_cmp(t13, t4, 8U);
    if (t15 == 1)
        goto LAB9;

LAB18:    t16 = (t0 + 5702);
    t18 = xsi_mem_cmp(t16, t4, 8U);
    if (t18 == 1)
        goto LAB10;

LAB19:    t19 = (t0 + 5710);
    t21 = xsi_mem_cmp(t19, t4, 8U);
    if (t21 == 1)
        goto LAB11;

LAB20:    t22 = (t0 + 5718);
    t24 = xsi_mem_cmp(t22, t4, 8U);
    if (t24 == 1)
        goto LAB12;

LAB21:    t25 = (t0 + 5726);
    t27 = xsi_mem_cmp(t25, t4, 8U);
    if (t27 == 1)
        goto LAB13;

LAB22:
LAB14:    xsi_set_current_line(39, ng0);

LAB5:    goto LAB3;

LAB6:    xsi_set_current_line(31, ng0);
    t28 = (t0 + 5734);
    t30 = (t0 + 3760);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    memcpy(t34, t28, 8U);
    xsi_driver_first_trans_fast(t30);
    goto LAB5;

LAB7:    xsi_set_current_line(32, ng0);
    t1 = (t0 + 5742);
    t4 = (t0 + 3760);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB8:    xsi_set_current_line(33, ng0);
    t1 = (t0 + 5750);
    t4 = (t0 + 3760);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB9:    xsi_set_current_line(34, ng0);
    t1 = (t0 + 5758);
    t4 = (t0 + 3760);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB10:    xsi_set_current_line(35, ng0);
    t1 = (t0 + 5766);
    t4 = (t0 + 3760);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB11:    xsi_set_current_line(36, ng0);
    t1 = (t0 + 5774);
    t4 = (t0 + 3760);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB12:    xsi_set_current_line(37, ng0);
    t1 = (t0 + 5782);
    t4 = (t0 + 3760);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB13:    xsi_set_current_line(38, ng0);
    t1 = (t0 + 5790);
    t4 = (t0 + 3760);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB23:;
}

static void work_a_3096070879_3212880686_p_1(char *t0)
{
    char t9[16];
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;

LAB0:    xsi_set_current_line(53, ng0);
    t1 = (t0 + 1152U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3664);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(54, ng0);
    t3 = (t0 + 1512U);
    t4 = *((char **)t3);
    t3 = (t0 + 3824);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t4, 8U);
    xsi_driver_first_trans_delta(t3, 8U, 8U, 0LL);
    xsi_set_current_line(55, ng0);
    t1 = (t0 + 1672U);
    t3 = *((char **)t1);
    t1 = (t0 + 5628U);
    t4 = ieee_p_2592010699_sub_1837678034_503743352(IEEE_P_2592010699, t9, t3, t1);
    t5 = (t9 + 12U);
    t10 = *((unsigned int *)t5);
    t11 = (1U * t10);
    t2 = (8U != t11);
    if (t2 == 1)
        goto LAB5;

LAB6:    t6 = (t0 + 3824);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t12 = (t8 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t4, 8U);
    xsi_driver_first_trans_delta(t6, 0U, 8U, 0LL);
    goto LAB3;

LAB5:    xsi_size_not_matching(8U, t11, 0);
    goto LAB6;

}

static void work_a_3096070879_3212880686_p_2(char *t0)
{
    char t3[16];
    char t4[16];
    char *t1;
    unsigned char t2;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;

LAB0:    xsi_set_current_line(68, ng0);
    t1 = (t0 + 1312U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3680);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(69, ng0);
    t5 = (t0 + 1672U);
    t6 = *((char **)t5);
    t5 = (t0 + 5628U);
    t7 = (t0 + 1672U);
    t8 = *((char **)t7);
    t7 = (t0 + 5628U);
    t9 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t4, t6, t5, t8, t7);
    t10 = ieee_p_3620187407_sub_436279890_3965413181(IEEE_P_3620187407, t3, t9, t4, 1);
    t11 = (t3 + 12U);
    t12 = *((unsigned int *)t11);
    t13 = (1U * t12);
    t14 = (8U != t13);
    if (t14 == 1)
        goto LAB5;

LAB6:    t15 = (t0 + 3888);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t10, 8U);
    xsi_driver_first_trans_fast(t15);
    goto LAB3;

LAB5:    xsi_size_not_matching(8U, t13, 0);
    goto LAB6;

}


extern void work_a_3096070879_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3096070879_3212880686_p_0,(void *)work_a_3096070879_3212880686_p_1,(void *)work_a_3096070879_3212880686_p_2};
	xsi_register_didat("work_a_3096070879_3212880686", "isim/matrix_tb_isim_beh.exe.sim/work/a_3096070879_3212880686.didat");
	xsi_register_executes(pe);
}

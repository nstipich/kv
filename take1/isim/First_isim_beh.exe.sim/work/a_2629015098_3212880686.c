/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "F:/Users/Nikola/take1/matrix.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;

unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
char *ieee_p_2592010699_sub_1837678034_503743352(char *, char *, char *, char *);
char *ieee_p_3620187407_sub_436279890_3965413181(char *, char *, char *, char *, int );
char *ieee_p_3620187407_sub_767668596_3965413181(char *, char *, char *, char *, char *, char *);


static void work_a_2629015098_3212880686_p_0(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    char *t5;
    int t6;
    char *t7;
    char *t8;
    int t9;
    char *t10;
    int t12;
    char *t13;
    int t15;
    char *t16;
    int t18;
    char *t19;
    int t21;
    char *t22;
    int t24;
    char *t25;
    int t27;
    char *t28;
    char *t30;
    char *t31;
    char *t32;
    char *t33;
    char *t34;

LAB0:    xsi_set_current_line(34, ng0);
    t1 = (t0 + 1152U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3808);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(35, ng0);
    t3 = (t0 + 1672U);
    t4 = *((char **)t3);
    t3 = (t0 + 5875);
    t6 = xsi_mem_cmp(t3, t4, 8U);
    if (t6 == 1)
        goto LAB6;

LAB15:    t7 = (t0 + 5883);
    t9 = xsi_mem_cmp(t7, t4, 8U);
    if (t9 == 1)
        goto LAB7;

LAB16:    t10 = (t0 + 5891);
    t12 = xsi_mem_cmp(t10, t4, 8U);
    if (t12 == 1)
        goto LAB8;

LAB17:    t13 = (t0 + 5899);
    t15 = xsi_mem_cmp(t13, t4, 8U);
    if (t15 == 1)
        goto LAB9;

LAB18:    t16 = (t0 + 5907);
    t18 = xsi_mem_cmp(t16, t4, 8U);
    if (t18 == 1)
        goto LAB10;

LAB19:    t19 = (t0 + 5915);
    t21 = xsi_mem_cmp(t19, t4, 8U);
    if (t21 == 1)
        goto LAB11;

LAB20:    t22 = (t0 + 5923);
    t24 = xsi_mem_cmp(t22, t4, 8U);
    if (t24 == 1)
        goto LAB12;

LAB21:    t25 = (t0 + 5931);
    t27 = xsi_mem_cmp(t25, t4, 8U);
    if (t27 == 1)
        goto LAB13;

LAB22:
LAB14:    xsi_set_current_line(44, ng0);

LAB5:    goto LAB3;

LAB6:    xsi_set_current_line(36, ng0);
    t28 = (t0 + 5939);
    t30 = (t0 + 3920);
    t31 = (t30 + 56U);
    t32 = *((char **)t31);
    t33 = (t32 + 56U);
    t34 = *((char **)t33);
    memcpy(t34, t28, 8U);
    xsi_driver_first_trans_fast(t30);
    goto LAB5;

LAB7:    xsi_set_current_line(37, ng0);
    t1 = (t0 + 5947);
    t4 = (t0 + 3920);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB8:    xsi_set_current_line(38, ng0);
    t1 = (t0 + 5955);
    t4 = (t0 + 3920);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB9:    xsi_set_current_line(39, ng0);
    t1 = (t0 + 5963);
    t4 = (t0 + 3920);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB10:    xsi_set_current_line(40, ng0);
    t1 = (t0 + 5971);
    t4 = (t0 + 3920);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB11:    xsi_set_current_line(41, ng0);
    t1 = (t0 + 5979);
    t4 = (t0 + 3920);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB12:    xsi_set_current_line(42, ng0);
    t1 = (t0 + 5987);
    t4 = (t0 + 3920);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB13:    xsi_set_current_line(43, ng0);
    t1 = (t0 + 5995);
    t4 = (t0 + 3920);
    t5 = (t4 + 56U);
    t7 = *((char **)t5);
    t8 = (t7 + 56U);
    t10 = *((char **)t8);
    memcpy(t10, t1, 8U);
    xsi_driver_first_trans_fast(t4);
    goto LAB5;

LAB23:;
}

static void work_a_2629015098_3212880686_p_1(char *t0)
{
    char t9[16];
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned int t10;
    unsigned int t11;
    char *t12;
    char *t13;

LAB0:    xsi_set_current_line(58, ng0);
    t1 = (t0 + 1152U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3824);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(59, ng0);
    t3 = (t0 + 1672U);
    t4 = *((char **)t3);
    t3 = (t0 + 3984);
    t5 = (t3 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t4, 8U);
    xsi_driver_first_trans_delta(t3, 8U, 8U, 0LL);
    xsi_set_current_line(60, ng0);
    t1 = (t0 + 1832U);
    t3 = *((char **)t1);
    t1 = (t0 + 5832U);
    t4 = ieee_p_2592010699_sub_1837678034_503743352(IEEE_P_2592010699, t9, t3, t1);
    t5 = (t9 + 12U);
    t10 = *((unsigned int *)t5);
    t11 = (1U * t10);
    t2 = (8U != t11);
    if (t2 == 1)
        goto LAB5;

LAB6:    t6 = (t0 + 3984);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t12 = (t8 + 56U);
    t13 = *((char **)t12);
    memcpy(t13, t4, 8U);
    xsi_driver_first_trans_delta(t6, 0U, 8U, 0LL);
    goto LAB3;

LAB5:    xsi_size_not_matching(8U, t11, 0);
    goto LAB6;

}

static void work_a_2629015098_3212880686_p_2(char *t0)
{
    char t7[16];
    char t8[16];
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t9;
    char *t10;
    char *t11;
    char *t12;
    char *t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned char t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;

LAB0:    xsi_set_current_line(73, ng0);
    t1 = (t0 + 1312U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3840);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(74, ng0);
    t3 = (t0 + 1512U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t6 = (t5 == (unsigned char)2);
    if (t6 != 0)
        goto LAB5;

LAB7:
LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(75, ng0);
    t3 = (t0 + 1832U);
    t9 = *((char **)t3);
    t3 = (t0 + 5832U);
    t10 = (t0 + 1832U);
    t11 = *((char **)t10);
    t10 = (t0 + 5832U);
    t12 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t8, t9, t3, t11, t10);
    t13 = ieee_p_3620187407_sub_436279890_3965413181(IEEE_P_3620187407, t7, t12, t8, 1);
    t14 = (t7 + 12U);
    t15 = *((unsigned int *)t14);
    t16 = (1U * t15);
    t17 = (8U != t16);
    if (t17 == 1)
        goto LAB8;

LAB9:    t18 = (t0 + 4048);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    memcpy(t22, t13, 8U);
    xsi_driver_first_trans_fast(t18);
    goto LAB6;

LAB8:    xsi_size_not_matching(8U, t16, 0);
    goto LAB9;

}


extern void work_a_2629015098_3212880686_init()
{
	static char *pe[] = {(void *)work_a_2629015098_3212880686_p_0,(void *)work_a_2629015098_3212880686_p_1,(void *)work_a_2629015098_3212880686_p_2};
	xsi_register_didat("work_a_2629015098_3212880686", "isim/First_isim_beh.exe.sim/work/a_2629015098_3212880686.didat");
	xsi_register_executes(pe);
}

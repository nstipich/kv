--Standard includes
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
--Entity definiton
entity First is
    Port ( A : out  STD_LOGIC_VECTOR (16 downto 0); -- maps to pins 0 to 16 of port A
              clk : in STD_LOGIC); --clock signal
end First;
 
--architecture definition
architecture Behavioral of First is
 --Interial signals
 signal counter : STD_LOGIC_VECTOR(8 downto 0) := (others => '0'); --counter to control state and delay
 signal row : STD_LOGIC_VECTOR(7 downto 0) := "11111110";               --signal for LED rows
 signal col : STD_LOGIC_VECTOR(7 downto 0) := "00000001";               --signal for LED columns
begin

    --Process Definition
    count: process(clk)
   begin
        -- triggers action on rising edge of clock signal
     if rising_edge(clk) then
        --increment counter
       counter <= counter+1;
         --clock period is 31.25ns, counter is 9 bits, should scan whole matrix in < 1ms
            --trigger each time counter rolls over back to zero
            if counter = 0 then
                -- Left Rotate col
                col <= col(6 downto 0) & col(7);
                -- Trigger when last column becomes active
                if col = "10000000" then
                    -- Left rotate row
                    row <= row(6 downto 0) & row(7);
                end if;
            end if;
            --copy signals to outputs
            A(7 downto 0) <= row;
            A(15 downto 8 ) <= col;
     end if;
   end process;
 
 
end Behavioral;
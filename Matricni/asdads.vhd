--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:18:27 01/17/2019
-- Design Name:   
-- Module Name:   F:/Users/Nikola/Matricni/asdads.vhd
-- Project Name:  Matricni
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: matricni
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY asdads IS
END asdads;
 
ARCHITECTURE behavior OF asdads IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT matricni
    PORT(
         A : OUT  std_logic_vector(5 downto 0);
         Enabled : OUT  std_logic;
			Brojac: out STD_LOGIC_VECTOR (5 downto 0);
			B: OUT std_logic_vector(63 downto 0);
         clk : IN  std_logic;
         clk2 : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal clk2 : std_logic := '0';

 	--Outputs
   signal A : std_logic_vector(5 downto 0);
	signal B : std_logic_vector(63 downto 0);
	signal Brojac: STD_LOGIC_VECTOR (5 downto 0);
   signal Enabled : std_logic;

   -- Clock period definitions
   constant clk_period : time := 1 ns;
   constant clk2_period : time := 5 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: matricni PORT MAP (
          A => A,
			 B=>B,
			 Brojac=>Brojac,
          Enabled => Enabled,
          clk => clk,
          clk2 => clk2
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   clk2_process :process
   begin
		clk2 <= '0';
		wait for clk2_period/2;
		clk2 <= '1';
		wait for clk2_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;

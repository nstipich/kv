
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
ENTITY freqDivGe_tb IS
END freqDivGe_tb;
 
ARCHITECTURE behavior OF freqDivGe_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT freqDivGen
    PORT(
         clk : IN  std_logic;
         clk_o : buffer  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';

 	--Outputs
   signal clk_o : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
   constant clk_o_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: freqDivGen PORT MAP (
          clk => clk,
          clk_o => clk_o
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   clk_o_process :process
   begin
		clk_o <= '0';
		wait for clk_o_period/2;
		clk_o <= '1';
		wait for clk_o_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;

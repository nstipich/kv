/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "F:/Users/Nikola/Matricni/matricni.vhd";
extern char *IEEE_P_2592010699;
extern char *IEEE_P_3620187407;
extern char *IEEE_P_1242562249;

char *ieee_p_1242562249_sub_180853171_1035706684(char *, char *, int , int );
unsigned char ieee_p_2592010699_sub_1744673427_503743352(char *, char *, unsigned int , unsigned int );
char *ieee_p_3620187407_sub_674691591_3965413181(char *, char *, char *, char *, unsigned char );
char *ieee_p_3620187407_sub_767668596_3965413181(char *, char *, char *, char *, char *, char *);


static void work_a_3046441357_3212880686_p_0(char *t0)
{
    char t3[16];
    char t4[16];
    char *t1;
    unsigned char t2;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    char *t11;
    unsigned int t12;
    unsigned int t13;
    unsigned char t14;
    char *t15;
    char *t16;
    char *t17;
    char *t18;
    char *t19;

LAB0:    xsi_set_current_line(39, ng0);
    t1 = (t0 + 1792U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 4640);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(40, ng0);
    t5 = (t0 + 2632U);
    t6 = *((char **)t5);
    t5 = (t0 + 7612U);
    t7 = (t0 + 2632U);
    t8 = *((char **)t7);
    t7 = (t0 + 7612U);
    t9 = ieee_p_3620187407_sub_767668596_3965413181(IEEE_P_3620187407, t4, t6, t5, t8, t7);
    t10 = ieee_p_3620187407_sub_674691591_3965413181(IEEE_P_3620187407, t3, t9, t4, (unsigned char)3);
    t11 = (t3 + 12U);
    t12 = *((unsigned int *)t11);
    t13 = (1U * t12);
    t14 = (64U != t13);
    if (t14 == 1)
        goto LAB5;

LAB6:    t15 = (t0 + 4736);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    t18 = (t17 + 56U);
    t19 = *((char **)t18);
    memcpy(t19, t10, 64U);
    xsi_driver_first_trans_fast(t15);
    goto LAB3;

LAB5:    xsi_size_not_matching(64U, t13, 0);
    goto LAB6;

}

static void work_a_3046441357_3212880686_p_1(char *t0)
{
    char t26[16];
    char t28[16];
    char t38[16];
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    char *t5;
    int t6;
    int t7;
    unsigned int t8;
    unsigned int t9;
    unsigned int t10;
    unsigned char t11;
    unsigned char t12;
    char *t13;
    char *t14;
    unsigned int t15;
    unsigned int t16;
    unsigned int t17;
    char *t18;
    char *t19;
    int t20;
    unsigned int t21;
    unsigned int t22;
    unsigned int t23;
    unsigned char t24;
    char *t25;
    char *t27;
    char *t29;
    char *t30;
    int t31;
    unsigned int t32;
    unsigned char t33;
    char *t34;
    char *t35;
    char *t36;
    char *t37;
    char *t39;
    int t40;
    int t41;
    int t42;
    int t43;
    char *t44;
    char *t46;
    char *t47;
    char *t48;
    char *t49;
    char *t50;

LAB0:    xsi_set_current_line(50, ng0);
    t1 = (t0 + 1632U);
    t2 = ieee_p_2592010699_sub_1744673427_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 4656);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(51, ng0);
    t3 = (t0 + 2632U);
    t4 = *((char **)t3);
    t3 = (t0 + 3088U);
    t5 = *((char **)t3);
    t6 = *((int *)t5);
    t7 = (t6 - 63);
    t8 = (t7 * -1);
    xsi_vhdl_check_range_of_index(63, 0, -1, t6);
    t9 = (1U * t8);
    t10 = (0 + t9);
    t3 = (t4 + t10);
    t11 = *((unsigned char *)t3);
    t12 = (t11 == (unsigned char)3);
    if (t12 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(86, ng0);
    t1 = (t0 + 2792U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t1 = (t0 + 5120);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t13 = (t5 + 56U);
    t14 = *((char **)t13);
    *((unsigned char *)t14) = t2;
    xsi_driver_first_trans_fast_port(t1);

LAB6:    xsi_set_current_line(88, ng0);
    t1 = (t0 + 2632U);
    t3 = *((char **)t1);
    t1 = (t0 + 5184);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t13 = (t5 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t3, 64U);
    xsi_driver_first_trans_fast_port(t1);
    xsi_set_current_line(89, ng0);
    t1 = (t0 + 3088U);
    t3 = *((char **)t1);
    t6 = *((int *)t3);
    t1 = ieee_p_1242562249_sub_180853171_1035706684(IEEE_P_1242562249, t26, t6, 6);
    t4 = (t0 + 5248);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 6U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(90, ng0);
    t1 = (t0 + 3088U);
    t3 = *((char **)t1);
    t6 = *((int *)t3);
    t7 = (t6 + 1);
    t1 = (t0 + 3088U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = t7;
    xsi_set_current_line(91, ng0);
    t1 = (t0 + 3088U);
    t3 = *((char **)t1);
    t6 = *((int *)t3);
    t2 = (t6 > 63);
    if (t2 != 0)
        goto LAB53;

LAB55:
LAB54:    goto LAB3;

LAB5:    xsi_set_current_line(53, ng0);
    t13 = (t0 + 2472U);
    t14 = *((char **)t13);
    t15 = (7 - 6);
    t16 = (t15 * 1U);
    t17 = (0 + t16);
    t13 = (t14 + t17);
    t18 = (t0 + 2472U);
    t19 = *((char **)t18);
    t20 = (7 - 7);
    t21 = (t20 * -1);
    t22 = (1U * t21);
    t23 = (0 + t22);
    t18 = (t19 + t23);
    t24 = *((unsigned char *)t18);
    t27 = ((IEEE_P_2592010699) + 4024);
    t29 = (t28 + 0U);
    t30 = (t29 + 0U);
    *((int *)t30) = 6;
    t30 = (t29 + 4U);
    *((int *)t30) = 0;
    t30 = (t29 + 8U);
    *((int *)t30) = -1;
    t31 = (0 - 6);
    t32 = (t31 * -1);
    t32 = (t32 + 1);
    t30 = (t29 + 12U);
    *((unsigned int *)t30) = t32;
    t25 = xsi_base_array_concat(t25, t26, t27, (char)97, t13, t28, (char)99, t24, (char)101);
    t32 = (7U + 1U);
    t33 = (8U != t32);
    if (t33 == 1)
        goto LAB8;

LAB9:    t30 = (t0 + 4800);
    t34 = (t30 + 56U);
    t35 = *((char **)t34);
    t36 = (t35 + 56U);
    t37 = *((char **)t36);
    memcpy(t37, t25, 8U);
    xsi_driver_first_trans_fast(t30);
    xsi_set_current_line(55, ng0);
    t1 = (t0 + 2472U);
    t3 = *((char **)t1);
    t1 = (t0 + 7596U);
    t4 = (t0 + 7748);
    t13 = (t26 + 0U);
    t14 = (t13 + 0U);
    *((int *)t14) = 0;
    t14 = (t13 + 4U);
    *((int *)t14) = 7;
    t14 = (t13 + 8U);
    *((int *)t14) = 1;
    t6 = (7 - 0);
    t8 = (t6 * 1);
    t8 = (t8 + 1);
    t14 = (t13 + 12U);
    *((unsigned int *)t14) = t8;
    t2 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t3, t1, t4, t26);
    if (t2 != 0)
        goto LAB10;

LAB12:
LAB11:    xsi_set_current_line(59, ng0);
    t1 = (t0 + 2472U);
    t3 = *((char **)t1);
    t1 = (t0 + 7756);
    t6 = xsi_mem_cmp(t1, t3, 8U);
    if (t6 == 1)
        goto LAB16;

LAB25:    t5 = (t0 + 7764);
    t7 = xsi_mem_cmp(t5, t3, 8U);
    if (t7 == 1)
        goto LAB17;

LAB26:    t14 = (t0 + 7772);
    t20 = xsi_mem_cmp(t14, t3, 8U);
    if (t20 == 1)
        goto LAB18;

LAB27:    t19 = (t0 + 7780);
    t31 = xsi_mem_cmp(t19, t3, 8U);
    if (t31 == 1)
        goto LAB19;

LAB28:    t27 = (t0 + 7788);
    t40 = xsi_mem_cmp(t27, t3, 8U);
    if (t40 == 1)
        goto LAB20;

LAB29:    t30 = (t0 + 7796);
    t41 = xsi_mem_cmp(t30, t3, 8U);
    if (t41 == 1)
        goto LAB21;

LAB30:    t35 = (t0 + 7804);
    t42 = xsi_mem_cmp(t35, t3, 8U);
    if (t42 == 1)
        goto LAB22;

LAB31:    t37 = (t0 + 7812);
    t43 = xsi_mem_cmp(t37, t3, 8U);
    if (t43 == 1)
        goto LAB23;

LAB32:
LAB24:    xsi_set_current_line(68, ng0);

LAB15:    xsi_set_current_line(71, ng0);
    t1 = (t0 + 2312U);
    t3 = *((char **)t1);
    t1 = (t0 + 7844);
    t6 = xsi_mem_cmp(t1, t3, 8U);
    if (t6 == 1)
        goto LAB35;

LAB44:    t5 = (t0 + 7852);
    t7 = xsi_mem_cmp(t5, t3, 8U);
    if (t7 == 1)
        goto LAB36;

LAB45:    t14 = (t0 + 7860);
    t20 = xsi_mem_cmp(t14, t3, 8U);
    if (t20 == 1)
        goto LAB37;

LAB46:    t19 = (t0 + 7868);
    t31 = xsi_mem_cmp(t19, t3, 8U);
    if (t31 == 1)
        goto LAB38;

LAB47:    t27 = (t0 + 7876);
    t40 = xsi_mem_cmp(t27, t3, 8U);
    if (t40 == 1)
        goto LAB39;

LAB48:    t30 = (t0 + 7884);
    t41 = xsi_mem_cmp(t30, t3, 8U);
    if (t41 == 1)
        goto LAB40;

LAB49:    t35 = (t0 + 7892);
    t42 = xsi_mem_cmp(t35, t3, 8U);
    if (t42 == 1)
        goto LAB41;

LAB50:    t37 = (t0 + 7900);
    t43 = xsi_mem_cmp(t37, t3, 8U);
    if (t43 == 1)
        goto LAB42;

LAB51:
LAB43:    xsi_set_current_line(80, ng0);

LAB34:    xsi_set_current_line(82, ng0);
    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t1 = (t0 + 5056);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t13 = (t5 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t3, 3U);
    xsi_driver_first_trans_delta(t1, 3U, 3U, 0LL);
    xsi_set_current_line(83, ng0);
    t1 = (t0 + 2152U);
    t3 = *((char **)t1);
    t1 = (t0 + 5056);
    t4 = (t1 + 56U);
    t5 = *((char **)t4);
    t13 = (t5 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t3, 3U);
    xsi_driver_first_trans_delta(t1, 0U, 3U, 0LL);
    xsi_set_current_line(84, ng0);
    t1 = (t0 + 5120);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    *((unsigned char *)t13) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB6;

LAB8:    xsi_size_not_matching(8U, t32, 0);
    goto LAB9;

LAB10:    xsi_set_current_line(57, ng0);
    t14 = (t0 + 2312U);
    t18 = *((char **)t14);
    t8 = (7 - 6);
    t9 = (t8 * 1U);
    t10 = (0 + t9);
    t14 = (t18 + t10);
    t19 = (t0 + 2312U);
    t25 = *((char **)t19);
    t7 = (7 - 7);
    t15 = (t7 * -1);
    t16 = (1U * t15);
    t17 = (0 + t16);
    t19 = (t25 + t17);
    t11 = *((unsigned char *)t19);
    t29 = ((IEEE_P_2592010699) + 4024);
    t30 = (t38 + 0U);
    t34 = (t30 + 0U);
    *((int *)t34) = 6;
    t34 = (t30 + 4U);
    *((int *)t34) = 0;
    t34 = (t30 + 8U);
    *((int *)t34) = -1;
    t20 = (0 - 6);
    t21 = (t20 * -1);
    t21 = (t21 + 1);
    t34 = (t30 + 12U);
    *((unsigned int *)t34) = t21;
    t27 = xsi_base_array_concat(t27, t28, t29, (char)97, t14, t38, (char)99, t11, (char)101);
    t21 = (7U + 1U);
    t12 = (8U != t21);
    if (t12 == 1)
        goto LAB13;

LAB14:    t34 = (t0 + 4864);
    t35 = (t34 + 56U);
    t36 = *((char **)t35);
    t37 = (t36 + 56U);
    t39 = *((char **)t37);
    memcpy(t39, t27, 8U);
    xsi_driver_first_trans_fast(t34);
    goto LAB11;

LAB13:    xsi_size_not_matching(8U, t21, 0);
    goto LAB14;

LAB16:    xsi_set_current_line(60, ng0);
    t44 = (t0 + 7820);
    t46 = (t0 + 4928);
    t47 = (t46 + 56U);
    t48 = *((char **)t47);
    t49 = (t48 + 56U);
    t50 = *((char **)t49);
    memcpy(t50, t44, 3U);
    xsi_driver_first_trans_fast(t46);
    goto LAB15;

LAB17:    xsi_set_current_line(61, ng0);
    t1 = (t0 + 7823);
    t4 = (t0 + 4928);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB15;

LAB18:    xsi_set_current_line(62, ng0);
    t1 = (t0 + 7826);
    t4 = (t0 + 4928);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB15;

LAB19:    xsi_set_current_line(63, ng0);
    t1 = (t0 + 7829);
    t4 = (t0 + 4928);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB15;

LAB20:    xsi_set_current_line(64, ng0);
    t1 = (t0 + 7832);
    t4 = (t0 + 4928);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB15;

LAB21:    xsi_set_current_line(65, ng0);
    t1 = (t0 + 7835);
    t4 = (t0 + 4928);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB15;

LAB22:    xsi_set_current_line(66, ng0);
    t1 = (t0 + 7838);
    t4 = (t0 + 4928);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB15;

LAB23:    xsi_set_current_line(67, ng0);
    t1 = (t0 + 7841);
    t4 = (t0 + 4928);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB15;

LAB33:;
LAB35:    xsi_set_current_line(72, ng0);
    t44 = (t0 + 7908);
    t46 = (t0 + 4992);
    t47 = (t46 + 56U);
    t48 = *((char **)t47);
    t49 = (t48 + 56U);
    t50 = *((char **)t49);
    memcpy(t50, t44, 3U);
    xsi_driver_first_trans_fast(t46);
    goto LAB34;

LAB36:    xsi_set_current_line(73, ng0);
    t1 = (t0 + 7911);
    t4 = (t0 + 4992);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB34;

LAB37:    xsi_set_current_line(74, ng0);
    t1 = (t0 + 7914);
    t4 = (t0 + 4992);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB34;

LAB38:    xsi_set_current_line(75, ng0);
    t1 = (t0 + 7917);
    t4 = (t0 + 4992);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB34;

LAB39:    xsi_set_current_line(76, ng0);
    t1 = (t0 + 7920);
    t4 = (t0 + 4992);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB34;

LAB40:    xsi_set_current_line(77, ng0);
    t1 = (t0 + 7923);
    t4 = (t0 + 4992);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB34;

LAB41:    xsi_set_current_line(78, ng0);
    t1 = (t0 + 7926);
    t4 = (t0 + 4992);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB34;

LAB42:    xsi_set_current_line(79, ng0);
    t1 = (t0 + 7929);
    t4 = (t0 + 4992);
    t5 = (t4 + 56U);
    t13 = *((char **)t5);
    t14 = (t13 + 56U);
    t18 = *((char **)t14);
    memcpy(t18, t1, 3U);
    xsi_driver_first_trans_fast(t4);
    goto LAB34;

LAB52:;
LAB53:    xsi_set_current_line(92, ng0);
    t1 = (t0 + 3088U);
    t4 = *((char **)t1);
    t1 = (t4 + 0);
    *((int *)t1) = 0;
    goto LAB54;

}


extern void work_a_3046441357_3212880686_init()
{
	static char *pe[] = {(void *)work_a_3046441357_3212880686_p_0,(void *)work_a_3046441357_3212880686_p_1};
	xsi_register_didat("work_a_3046441357_3212880686", "isim/asdads_isim_beh.exe.sim/work/a_3046441357_3212880686.didat");
	xsi_register_executes(pe);
}

--Standard includes
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
USE ieee.numeric_std.ALL;

-- Za ucf 	A0-A7 row pins
--				A8-A15 col pins

--Entity definiton
entity matricni is
    Port ( A : out  STD_LOGIC_VECTOR (5 downto 0); -- maps to pins 0 to 16 of port A
			 Enabled: out STD_LOGIC;
			 Brojac: out STD_LOGIC_VECTOR (5 downto 0);
			 B:out STD_LOGIC_VECTOR(63 downto 0);
           clk,clk2: in STD_LOGIC); --clock signal
end matricni;
 
 

architecture Behavioral of matricni is
	 signal row : STD_LOGIC_VECTOR(2 downto 0) := "000"; 
	 signal col : STD_LOGIC_VECTOR(2 downto 0) := "000";
	 signal row1 : STD_LOGIC_VECTOR(7 downto 0) := "00000001";               --signal for LED rows
	 signal col1 : STD_LOGIC_VECTOR(7 downto 0) := "00000001";    
	 signal var: STD_LOGIC_VECTOR(63 downto 0):="0000000000000000000000000000000000000000000000000000000000000001";
	 signal E: STD_LOGIC:='1';
--	 signal clk2:STD_LOGIC;
--	 signal clk3:STD_LOGIC;
begin



--s1:entity work.freqDivGen GENERIC MAP(24000000) port map(clk,clk2);
--s2:entity work.freqDivGen GENERIC MAP(600) port map(clk,clk3);
   process(clk2)
   begin
      if rising_edge(clk2) then --1hz
		  var<=((var+var)+'1');
      end if;
   end process;




	 process(clk)
			variable counter : integer range 0 to 64:=0;
		  begin
			  if rising_edge(clk) then
					if(var(counter)='1') then
								 -- Left Rotate col
								 col1 <= col1(6 downto 0) & col1(7);
								 -- Trigger when last column becomes active
								 if col1 = "10000000" then
									  -- Left rotate row
									  row1 <= row1(6 downto 0) & row1(7);
								 end if;
								case col1 is 
									when "10000000" => col <= "111";
									when "01000000" => col <= "110";	
									when "00100000" => col <= "101";
									when "00010000" => col <= "100";
									when "00001000" => col <= "011";
									when "00000100" => col <= "010";	
									when "00000010" => col <= "001";
									when "00000001" => col <= "000";
									when others => NULL;
								end case;
								
								case row1 is 
									when "10000000" => row <= "111";
									when "01000000" => row <= "110";	
									when "00100000" => row <= "101";
									when "00010000" => row <= "100";
									when "00001000" => row <= "011";
									when "00000100" => row <= "010";	
									when "00000010" => row <= "001";
									when "00000001" => row <= "000";
									when others => NULL;							
								end case;
							A(2 downto 0) <= row;
							A(5 downto 3) <= col;
							Enabled <= '0';
					else
						Enabled<=E;
				   end if;
				  B<=var;
				  Brojac<= std_logic_vector(to_unsigned(counter, 6));
				  counter := counter +1;
				  if(counter > 63) then
						counter:=0;
				  end if;
			  end if;
	end process;
end Behavioral;


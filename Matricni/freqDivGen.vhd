--Standard includes
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity freqDivGen is
	generic(Fclk:natural:=40000000);
	port(clk:IN STD_LOGIC:='0';
			clk_o:buffer STD_LOGIC:='0');
end freqDivGen;

architecture Behavioral of freqDivGen is
	begin
		process(clk)
			variable temp:integer range 0 to Fclk/2:=0;
			begin
				if (clk'event and clk='1') then
					temp:=temp+1;
					if(temp>=Fclk/2)then
						clk_o<=not clk_o;
						temp:=0;
					end if;
				end if;
		end process;

	end Behavioral;